import { Controller, Get, Param } from '@nestjs/common';
import { PlayersService } from './players.service';

@Controller('players')
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}

  @Get(':world/:username')
  async getPlayerByUsername(@Param('world') world: string, @Param('username') username: string) {
    return await this.playersService.getPlayer(world, username);
  }
}
