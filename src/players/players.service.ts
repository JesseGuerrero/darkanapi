import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Document, Model } from 'mongoose';
import { WorldDataService } from 'src/world/world.service';

@Injectable()
export class PlayersService {
  constructor(
    @InjectModel('Account', 'lobbyConnection')
    private readonly accounts: Model<Document<any>>,
    private readonly worldData: WorldDataService,
  ) {}

  async getPlayer(world: string, displayName: string) {
    const account = await this.accounts.collection.findOne(
      { displayName: displayName },
      {
        projection: {
          _id: 0,
          username: 1,
        },
      },
    );
    if (!account) throw new NotFoundException('Account not found.');
    const player = await this.worldData.getCollection(world, 'players').findOne(
      { username: account.username },
      {
        projection: {
          _id: 0,
          'inventory.items': 1,
          'equipment.items': 1,
          'bank.bankTabs': 1,
          'skills.level': 1,
          'skills.xp': 1,
          npcKills: 1,
          variousCounter: 1,
          timePlayed: 1,
        },
      },
    );
    if (!player) throw new NotFoundException('Player not found.');
    return player;
  }
}
