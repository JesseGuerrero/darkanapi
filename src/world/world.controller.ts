import { Controller, Get } from '@nestjs/common';
import { WorldDataService } from './world.service';

@Controller('world')
export class WorldController {
  constructor(private readonly worlds: WorldDataService) {}

  @Get()
  async getWorlds() {
    return this.worlds.getWorlds();
  }

  @Get('online')
  async getPlayersOnline() {
    return await this.worlds.getAllPlayersOnline();
  }
}
