import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
import { NestExpressApplication } from '@nestjs/platform-express';
import { useContainer } from 'class-validator';
//import { startDaemonTemporalHiscores } from './temporal_hiscores/temporal_daemon';

const PORT = process.env.PORT ?? 8080;
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.enableCors();
  app.setGlobalPrefix('v1');
  app.useGlobalPipes(new ValidationPipe());
  app.useStaticAssets(join(__dirname, '..', 'public'), {
    //http://localhost:8443/public/ge-icons/2.png
    index: false,
    prefix: '/public',
  });
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  //startDaemonTemporalHiscores();

  await app.listen(PORT);
}
bootstrap();
