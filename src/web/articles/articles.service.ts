import { Injectable } from '@nestjs/common';
import { Article } from './schema/article.schema';
import { Document, Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class ArticlesService {
  constructor(
    @InjectModel(Article.name, 'lobbyConnection')
    private readonly articles: Model<Document<Article>>,
  ) {}

  async create(article: Article) {
    return await this.articles.create(article);
  }

  async update(id: string, article: Article) {
    return await this.articles.findByIdAndUpdate(id, article).lean();
  }

  async delete(id: string) {
    return this.articles.findByIdAndDelete(id);
  }

  async getCount() {
    return await this.articles.countDocuments();
  }

  async get(page = 1, limit = 6, type = 0) {
    return await this.articles.find(
      { type: type >= 1 && type <= 4 ? type : undefined },
      { createdAt: 'desc' },
      { skip: (page - 1) * limit, limit },
    );
  }

  async getById(id: string) {
    return await this.articles.findById(id);
  }

  async getBySlug(slug: string) {
    return await this.articles.findOne({ slug });
  }
}
