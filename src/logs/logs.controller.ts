import { Controller, Get, Param, Query } from '@nestjs/common';
import { LogsService } from './logs.service';

@Controller('logs')
export class LogsController {
  constructor(private readonly logsService: LogsService) {}

  @Get(':world/:logType')
  async getErrors(
    @Param('world') world: string,
    @Param('logType') logType: string,
    @Query('page') page = 1,
    @Query('limit') limit = 25,
  ) {
    return await this.logsService.getLogs(world, page, limit, logType.toUpperCase());
  }
}
