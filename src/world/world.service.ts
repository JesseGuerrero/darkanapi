import { Injectable, NotFoundException, Logger, OnModuleInit } from '@nestjs/common';
import { Collection, Connection } from 'mongoose';
import { ConfigService } from '@nestjs/config';
import { ModuleRef } from '@nestjs/core';
import { getConnectionToken } from '@nestjs/mongoose';

@Injectable()
export class WorldDataService implements OnModuleInit {
  private static readonly logger = new Logger(WorldDataService.name);
  private connections: Map<string, Connection> = new Map();

  constructor(
    private moduleRef: ModuleRef,
    private configService: ConfigService,
  ) {}

  onModuleInit() {
    WorldDataService.logger.log('Initializing WorldDataService connections');
    const prefix = 'WORLD_MONGO_URI_';
    for (const key in process.env) {
      if (key.startsWith(prefix)) {
        const connectionName = `${key.substring(prefix.length).toLowerCase()}Connection`;
        if (this.connections.has(connectionName)) continue;
        const connection = this.moduleRef.get<Connection>(`${getConnectionToken(`${connectionName}`)}`, {
          strict: false,
        });
        this.connections.set(connectionName, connection);
        WorldDataService.logger.log('Connection added: ' + connectionName);
      }
    }
  }

  getCollection(worldName: string, collection: string): Collection<Document> {
    const conn = this.connections.get(`${worldName.toLowerCase()}Connection`);
    if (!conn) {
      WorldDataService.logger.error(`Connection for ${worldName} not found`);
      throw new NotFoundException('World connection not found');
    }
    return conn.collection(collection);
  }

  getWorlds(): string[] {
    return [...this.connections.keys()].map(key => key.replace('Connection', ''));
  }

  async getAllPlayersOnline() {
    const res = await fetch('http://prod.darkan.org:4040/api/playersonline');
    const count = Number(await res.text());
    return { count };
  }
}
