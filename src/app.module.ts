import { Module } from '@nestjs/common';
import { AccountsModule } from './accounts/accounts.module';
import { LogsModule } from './logs/logs.module';
import { AuthModule } from './auth/auth.module';
import { PlayersModule } from './players/players.module';
import { HighscoresModule } from './highscores/highscores.module';
import { GEModule } from './ge/ge.module';
import { ArticlesModule } from './web/articles/articles.module';
// import { TemporalModule } from './temporal_hiscores/temporal_hiscore.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { WorldMongoModule } from './world/world-mongo.module';
import { WorldDataModule } from './world/world.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, envFilePath: '.env' }),
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      connectionName: 'lobbyConnection',
      useFactory: async (configService: ConfigService) => ({
        uri: configService.get<string>('LOBBY_MONGO_URI'),
      }),
      inject: [ConfigService],
    }),
    WorldMongoModule.forRoot(),
    GEModule,
    HighscoresModule,
    // TemporalModule,
    WorldDataModule,
    PlayersModule,
    ArticlesModule,
    AccountsModule,
    LogsModule,
    AuthModule,
  ],
})
export class AppModule {}
